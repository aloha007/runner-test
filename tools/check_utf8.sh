#!/bin/bash

check_utf8() {
    file_path=$1
    result=$(iconv -f UTF-8 -t UTF-8 "$file_path" 2>&1)

    if [ $? -eq 0 ]; then
        echo "$file_path is UTF-8 encoded."
    else
        echo "Error: $file_path is not UTF-8 encoded."
        exit 1
    fi
}



# 检查所有修改的文件
git diff --name-only $CI_COMMIT_BEFORE_SHA $CI_COMMIT_SHA | while read -r file; do
    check_utf8 "$file"
done
