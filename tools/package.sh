#!/bin/bash

# 设置存放打包文件文件夹
target_folder=$1

# 获取上一个tag
previous_tag=$(git describe --tags --abbrev=0 HEAD^)

#获取本次到上一个tag之间的所有commit并写入到文件
commits=$(git log --pretty=format:"%s" $previous_tag $CI_COMMIT_TAG)
echo $commits > ${target_folder}/commits.txt

#获取本次到上一个tag之间的变动文件
files=$(git diff --name-only $previous_tag $CI_COMMIT_TAG)

# 复制文件并保留目录结构
for file in $files; do
  mkdir -p "${target_folder}/$(dirname $file)"
  cp "$file" "${target_folder}/$file"
done

# 获取当前 Git 标签
package_name=$2

# 打印标签信息
echo "Current package_name: $package_name"

# 打包变动文件

tar -cvf ${package_name} ${target_folder}/
